import React from 'react';
// import { Link } from "react-router-dom";

//Styles
// import './AdminPannel.css';

//Components


//Assets
import backward from "../Assets/Icons/backward.svg";
import forward from "../Assets/Icons/forward.svg";
import copy from "../Assets/Icons/copy.svg";
import dots from "../Assets/Icons/dots.svg";


const PanelNavBar = () => {
    return (
        <div className="container-fluid">           

            <ul className="list-inline p-0 m-0">
                <li className="list-inline-item pt-2 float-right">
                    <a href="#">
                        <img src={backward} className="backwardIcon" alt="backward Icon"/>
                    </a>
                </li>
                <li className="list-inline-item p-0 m-0 float-right">
                    <a className="nav-link" href="#">
                        <img src={forward} className="forwardIcon" alt="forward"/>
                    </a>                    
                </li>
                <li className="list-inline-item p-0 m-0 float-right" role="presentation">
                    <a className="nav-link" href="#">
                    <img src={copy} className="copyIcon" alt="copy"/>
                    </a>
                </li>
                <li className="list-inline-item p-0 m-0 float-right">
                    <a className="nav-link" href="#">
                       <img src={dots} className="dotsIcon" alt="dots"/>
                    </a>                    
                </li>

                <li className="list-inline-item pt-3 float-left">
                    <p>گزارش ۱</p>
                </li>                
                
            </ul>
        </div>

    );
};
export default PanelNavBar;