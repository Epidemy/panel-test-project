import React from 'react';

//Styles
import './SideBarStyle.css';

//Assets
import database from "../Assets/Icons/database.svg";
import list from "../Assets/Icons/list.svg";
import laws from "../Assets/Icons/laws.svg";
import filter from "../Assets/Icons/filter.svg";
import design from "../Assets/Icons/design.svg";
import rating from "../Assets/Icons/rating.svg";
import operations from "../Assets/Icons/operations.svg";


const LeftSideBar = () => {
    return (
        <nav className="navbar d-flex flex-md-row-reverse accordion p-0 m-0">

            <div className="leftSidebar">
                <ul className="PanelTabs nav flex-column nav-pills p-0 m-0" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <li className="leftpanelListItem nav-item">
                        <a className="panelListTitle nav-link tablinks" id="v-pills-things-tab" data-toggle="pill" href="#v-pills-things" role="tab" aria-controls="v-pills-Busiesses" aria-selected="false">
                            <img src={list} className="listIcon" alt="list"/>
                            <p className="tabName">گزینه‌ها</p>                        
                        </a>
                    </li>
                    <li className="leftpanelListItem nav-item">
                        <a className="panelListTitle nav-link tablinks" id="v-pills-things-tab" data-toggle="pill" href="#v-pills-things" role="tab" aria-controls="v-pills-Busiesses" aria-selected="false">
                            <img src={laws} className="lawsIcon" alt="laws"/>
                            <p className="tabName">قوانین</p>
                        </a>
                    </li>
                    <li className="leftpanelListItem nav-item">
                        <a className="panelListTitle nav-link tablinks" id="v-pills-design-tab" data-toggle="pill" href="#v-pills-design" role="tab" aria-controls="v-pills-Request" aria-selected="false">    
                            <img src={operations} className="operationsIcon" alt="operations"/>
                            <p className="tabName">عملیات‌ها</p>
                        </a>
                    </li>
                    <li className="leftpanelListItem nav-item">
                        <a className="panelListTitle nav-link tablinks" id="v-pills-design-tab" data-toggle="pill" href="#v-pills-design" role="tab" aria-controls="v-pills-Request" aria-selected="false">    
                            <img src={filter} className="filterIcon" alt="filter"/>
                            <p className="tabName">فیلترها</p>
                        </a>
                    </li> 
                    <li className="leftpanelListItem nav-item">
                        <a className="panelListTitle nav-link tablinks" id="v-pills-design-tab" data-toggle="pill" href="#v-pills-design" role="tab" aria-controls="v-pills-Request" aria-selected="false">    
                            <img src={rating} className="ratingIcon" alt="rating"/>
                            <p className="tabName">رتبه‌ها</p>
                        </a>
                    </li>                         
                </ul>                
            </div>

        </nav>          

    );
};
export default LeftSideBar;