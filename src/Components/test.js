<div className="tab-content" id="v-pills-tabContent" style={{ width: "100%" }}>


                <div className="tab-pane fade show active" id="v-pills-myProfile" role="tabpanel" aria-labelledby="v-pills-myProfile-tab" >

                    <div id="minimal-tabs" style={{ width: "100%" }}>

                        <ul className="nav nav-tabs">
                            <li className="nav-item">
                                <a className="nav-link" role="tab" data-toggle="tab" href="#tab-1">ویرایش پروفایل</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" role="tab" data-toggle="tab" href="#tab-2">افزودن رزومه</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" role="tab" data-toggle="tab" href="#tab-3">مشاهده پروفایل و رزومه</a>
                            </li>
                        </ul>

                        <div className="tab-content" style={{font: "0.8em Vazir"}}>
                            <div id="tab-1" className="tab-pane editProfile" role="tabpanel" style={{ transition: "1s", marginTop: "1vh" }}>
                                <div className="row justify-content-center">
                                    <div className="card col-md-9 text-right shadow rounded-lg">
                                        <form className="form p-5 text-right">
                                            <div className="form-row">
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput">نام </label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control" id="formGroupExampleInput" placeholder="به فارسی" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput2">نام خانوادگی </label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control" id="formGroupExampleInput2" placeholder=" به فارسی" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-5" for="formGroupExampleInput2">جنسیت </label>
                                                        <div className="custom-control custom-radio ml-3">
                                                            <input type="radio" id="customRadio1" name="customRadio" className="custom-control-input" value="1" />
                                                            <label className="custom-control-label" for="customRadio1">مرد</label>
                                                        </div>
                                                        <div className="custom-control custom-radio">
                                                            <input type="radio" id="customRadio2" name="customRadio" className="custom-control-input" value="2" />
                                                            <label className="custom-control-label" for="customRadio2">زن</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="form-row">
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput">کدملی</label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control" id="formGroupExampleInput" placeholder=" ده رقمی" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput">تاریخ تولد</label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control text-left" id="formGroupExampleInput" placeholder="روز-ماه-سال" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput">ایمیل</label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control" id="formGroupExampleInput" placeholder="...@example.com" style={{ direction: "ltr" }} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-row">
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput2"> تلفن همراه </label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="09*********" style={{ direction: "ltr" }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput">تلفن ثابت</label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control text-left" id="formGroupExampleInput" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput2">فکس</label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control text-left" id="formGroupExampleInput2" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div className="form-row">

                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput2">نام کاربری</label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control text-left" id="formGroupExampleInput2" placeholder="@" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput2"> کد معرف</label>
                                                        <div className="col-sm-8">
                                                            <input type="text" className="form-control text-left" id="formGroupExampleInput2" placeholder="@" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-sm-2" for="formGroupExampleInput2">آدرس محل سکونت</label>
                                                <div className="col-md-10">
                                                    <div className="row pb-1">
                                                        <div className=" col-md-2">
                                                            <input type="text" className="form-control mx-md-1 form-control-md-lg form-control-input" id="region" placeholder="ایران" />
                                                        </div>
                                                        < div className="col-md-2" >
                                                            <select className="form-control mx-md-1 form-control-md-lg form-control-input" name="proviance" onChange={this.handleChange}>

                                                                <option id="inputState" selected="selected" disabled="disabled" value="استان">استان</option>
                                                                {Proviances.map(item =>
                                                                    <option key={item.id} value={item.provianceName}>{item.provianceName}</option>
                                                                )}

                                                            </select>
                                                        </div >

                                                        < div className="col-md-2" >
                                                            <select className="form-control mx-md-1 form-control-md-lg form-control-input" name="city" onChange={this.handleChange}>
                                                                <option selected="selected" disabled="disabled" value="شهر ">شهر</option>
                                                                {Cities.map(item =>
                                                                    <option key={item.id} value={item.city}>{item.city}</option>
                                                                )}
                                                            </select>
                                                        </div >
                                                        <div className=" col-md-2">
                                                            <input type="text" className="form-control mx-md-1 form-control-md-lg form-control-input" id="region" placeholder="منطقه" />
                                                        </div>

                                                        <div className=" col-md-2">
                                                            <input type="text" className="form-control mx-md-1 form-control-md-lg form-control-input" id="region" placeholder="خیابان" />
                                                        </div>

                                                        <div className=" col-md-2">
                                                            <input type="text" className="form-control mx-md-1 form-control-md-lg form-control-input" id="region" placeholder="کوچه" />
                                                        </div>
                                                    </div>

                                                    <div className="row pb-1">
                                                        <div className=" col-md-2">
                                                            <input type="text" className="form-control mx-md-1 form-control-md-lg form-control-input" id="region" placeholder="پلاک" />
                                                        </div>
                                                        <div className=" col-md-2">
                                                            <input type="text" className="form-control mx-md-1 form-control-md-lg form-control-input" id="region" placeholder="واحد" />
                                                        </div>
                                                        <div className="col-sm-8 float-left">
                                                            <textarea type="text" className="form-control" rows="1" id="formGroupExampleInput2" placeholder="توضیحات"></textarea>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            <div className="form-group row socialMediaInput">
                                                <label className="col-md-3" for="Social Media">آدرس شبکه های اجتماعی</label>
                                                <SocialMediasForm />

                                            </div>

                                            <div className="row justify-content-center">
                                                <div className="col-md-2">
                                                    <input type="file" name="file" id="file" className="inputfile" />
                                                    <label for="file"><span style={{ font: "1rem" }}>بارگذاری عکس</span></label>
                                                </div>
                                            </div>

                                            <div className="form-group row d-flex justify-content-center" style={{ marginTop: "2vh" }}>
                                                <div className="col-md-2">
                                                    <button type="submit" className="btn btn-primary" style={{ width: "100%" }}>ثبت اطلاعات</button>
                                                </div>
                                                <div className="col-sm-2">
                                                    <button type="submit" className="btn btn-danger" style={{ width: "100%" }}> انصراف</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div id="tab-2" className="tab-pane editResume" role="tabpanel" style={{ transition: "1s", marginTop: "0.5vh" }}>

                                <div className="row justify-content-center">
                                    <div className="col-md-3 justify-content-center rounded-lg">
                                        <select className="form-control mx-md-1 form-control-md-lg form-control-input" name="proviance" onChange={this.handleChange}>
                                            <option value="رستۀ شغلی" selected="selected" disabled="disabled">دسته بندی شغل خود را انتخاب کنید</option>
                                            <option value="Programmer"> ساختمانی </option>
                                            <option value="Programmer"> خدماتی </option>
                                            <option value="Programmer"> فروشگاهی </option>
                                            <option value="Programmer"> اداری </option>
                                            <option value="Programmer"> آموزشی </option>
                                            <option value="Programmer">  استخدامی</option>
                                            <option value="Programmer">  دایرکتوری </option>
                                            {/* <option id="inputState" selected="selected" disabled="disabled" value="استان">استان</option>
                                        {Proviances.map(item =>
                                            <option key={item.id} value={item.provianceName}>{item.provianceName}</option>
                                        )} */}

                                        </select>
                                    </div>
                                    <div className="col-md-3 justify-content-center rounded-lg">
                                        <select className="form-control mx-md-1 form-control-md-lg form-control-input" name="proviance" onChange={this.handleChange}>
                                            <option value="رستۀ شغلی" selected="selected" disabled="disabled">عنوان شغل خود را انتخاب کنید</option>
                                            <option value="Programmer">برنامه نویس</option>
                                            {/* <option id="inputState" selected="selected" disabled="disabled" value="استان">استان</option>
                                        {Proviances.map(item =>
                                            <option key={item.id} value={item.provianceName}>{item.provianceName}</option>
                                        )} */}

                                        </select>
                                    </div>
                                </div>

                                <div className="row justify-content-center m-0 p-0 ml-4">
                                    <div className="card ResumeForm col-md-11 text-right rounded-lg">
                                        <div className="x_pannel">
                                            <div className="x_title rounded-top">
                                                <h6 className="p-2">اطلاعات هویتی</h6>
                                            </div>
                                            <form className="form p-2">

                                                <div className="form-row">

                                                    <div className="col-sm-12 col-md-3">
                                                        <div className="form-group row">
                                                            <label className="col-md-4" for="name">نام </label>
                                                            <div className="col-md-8">
                                                                <input type="text" className="form-control" id="nameInput" placeholder="به فارسی" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-sm-12 col-md-3">
                                                        <div className="form-group row">
                                                            <label className="col-md-4" for="family">نام خانوادگی </label>
                                                            <div className="col-md-8">
                                                                <input type="text" className="form-control" id="lastNameInput" placeholder="به فارسی" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-sm-12 col-md-3">
                                                        <div className="form-group row">
                                                            <label className="col-md-4" for="birthday">سال تولد</label>
                                                            <div className="col-md-8">
                                                                <input type="text" className="form-control" id="birthdayInput" placeholder="" style={{ direction: "ltr" }} />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-sm-12 col-md-3">

                                                        <div className="form-group row">
                                                            <label className="col-md-4" for="email">ایمیل</label>
                                                            <div className="col-md-8">
                                                                <input type="text" className="form-control" id="emailInput" placeholder="...@example.com" style={{ direction: "ltr" }} />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="form-row">
                                                    <div className="col-sm-12 col-md-3">
                                                        <div className="form-group row">
                                                            <label className="col-md-4" for="phoneNumber">شماره تماس</label>
                                                            <div className="col-md-8">
                                                                <input type="text" className="form-control" id="phoneNumberInput" placeholder="09*********" style={{ direction: "ltr" }} />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-sm-12 col-md-3">
                                                        <div className="form-group row">
                                                            <label className="col-md-4" for="phoneNumber">شهر</label>
                                                            <div className="col-md-8">
                                                                <input type="text" className="form-control" id="phoneNumberInput" placeholder="" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-sm-12 col-md-3">
                                                        <div className="form-group row">
                                                            <label className="col-md-4" for="address">درباره من</label>
                                                            <div className="col-md-8">
                                                                <textarea type="text" className="form-control" rows="3" id="addressInput" placeholder="ماکزیمم 200 کلمه "></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-sm-12 col-md-3">
                                                        <div className="col-md-12">
                                                            <input type="file" name="file" id="file" className="inputfile" />
                                                            <label for="file"><span style={{ font: "1rem" }}>بارگذاری عکس</span></label>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div className="form-group row float-left" style={{ marginLeft: "1vw", marginTop: "1vh" }}>
                                                    <button type="submit" className="btn btn-secondary ml-1">ثبت اطلاعات</button>
                                                    <button type="submit" className="btn btn-secondary">انصراف</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>

                                    <div className="card ResumeForm col-md-11 text-right rounded-lg">
                                        <div className="x_pannel">
                                            <div className="x_title rounded-top">
                                                <h6 className="p-2">تحصیلات</h6>
                                            </div>
                                            <div className="p-3">
                                                <EducationForm />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card ResumeForm col-md-11 text-right rounded-lg">
                                        <div className="x_pannel">
                                            <div className="x_title rounded-top">
                                                <h6 className="p-2">مهارت ها</h6>
                                            </div>
                                            <form className="form p-2">
                                                <div className="form-group row">
                                                    <label className="col-md-3" for="address">مهارت های من</label>
                                                    <div className="col-md-9">
                                                        <textarea type="text" className="form-control" rows="2" id="addressInput" placeholder="ماکزیمم 12 مورد میتوانید وارد کنید"></textarea>
                                                    </div>
                                                </div>
                                                <div className="form-group row float-left" style={{ marginLeft: "1vw", marginTop: "1vh" }}>
                                                    {/* <div className="col-md-6"> */}
                                                    <button type="submit" className="btn btn-secondary ml-1">ثبت اطلاعات</button>
                                                    {/* </div> */}
                                                    {/* <div className="col-md-6"> */}
                                                    <button type="submit" className="btn btn-secondary">انصراف</button>
                                                    {/* </div> */}
                                                </div>

                                            </form>
                                        </div>
                                    </div>

                                    <div className="card ResumeForm col-md-11 text-right rounded-lg">
                                        <div className="x_pannel">
                                            <div className="x_title rounded-top">
                                                <h6 className="p-2">سوابق کاری</h6>
                                            </div>

                                            <div className="p-3">
                                                <JobExperience />
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div id="tab-3" className="tab-pane seeResume" role="tabpanel" style={{ transition: "1s", marginTop: "5vh", marginRight: "20vw" }}>
                                {/* <h1>پروفایل من</h1> */}
                            </div>
                        </div>

                    </div>

                    {/* <div className="d-flex align-items-center" style={{ display: this.state.FillProfileClicked ? 'none' : 'block', marginTop: this.state.FillProfileClicked ? "5vh" : "40vh", marginRight: "30vw" }}>
                        <button className="btn btn-warning btn-lg" onClick={this.handleClickFillProfile}><span>تکمیل پروفایل</span></button>
                    </div> */}

                </div>

                <div className="tab-pane fade" id="v-pills-Busiesses" role="tabpanel" aria-labelledby="v-pills-Busiesses-tab">
                    <div id="minimal-tabs">

                        <ul className="nav nav-tabs">
                            <li className="nav-item">
                                <a className="nav-link" role="tab" data-toggle="tab" href="#tab-addNewBusiness"> ایجاد کسب و کار جدید</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" role="tab" data-toggle="tab" href="#tab-manageMyBusinesses">مدیریت کسب و کارهای من</a>
                            </li>
                        </ul>

                        <div className="tab-content">
                            <div id="tab-addNewBusiness" className="tab-pane addNewBusiness" role="tabpanel" style={{ transition: "1s", marginTop: "1vh" }}>
                                <div className="row justify-content-center">
                                    <div className="card col-md-6 text-right rounded-lg">
                                        <form className="form p-5 text-right">

                                            <div className="form-group row">
                                                <label className="col-sm-4" for="formGroupExampleInput">رستۀ شغلی </label>
                                                <div className="col-sm-8">
                                                    <select className="form-control mx-md-1 form-control-md-lg form-control-input" name="proviance" onChange={this.handleChange}>
                                                        <option value="رستۀ شغلی" selected="selected" disabled="disabled">رستۀ شغلی خود را انتخاب کنید</option>
                                                        <option value="Programmer">برنامه نویس</option>
                                                        {/* <option id="inputState" selected="selected" disabled="disabled" value="استان">استان</option>
                                                                {Proviances.map(item =>
                                                                    <option key={item.id} value={item.provianceName}>{item.provianceName}</option>
                                                                )} */}
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-sm-4" for="formGroupExampleInput2">عنوان کسب و کار *</label>
                                                <div className="col-sm-8">
                                                    <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Another input" />
                                                </div>
                                            </div>


                                            <div className="form-group row">
                                                <label className="col-sm-4" for="formGroupExampleInput2">درباره کسب و کار</label>
                                                <div className="col-sm-8">
                                                    <textarea type="text" className="form-control" rows="3" id="formGroupExampleInput2" placeholder="Another input"></textarea>
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-sm-4" for="formGroupExampleInput2">آدرس </label>
                                                <div className="col-sm-8">
                                                    <textarea type="text" className="form-control" rows="3" id="formGroupExampleInput2" placeholder="Another input"></textarea>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-sm-4" for="formGroupExampleInput2">تماس</label>
                                                <div className="col-sm-8">
                                                    <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Another input" />
                                                </div>
                                            </div>

                                            <div className="form-group row">
                                                <label className="col-sm-4" for="formGroupExampleInput2">آخرین به روز رسانی</label>
                                                <div className="col-sm-8">
                                                    <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Another input" />
                                                </div>
                                            </div>

                                            <div className="form-row ">
                                                <div className="col-sm-12 col-md-4">
                                                    <div className="col-md-12">
                                                        <input type="file" name="file" id="file" className="inputfile" />
                                                        <label for="file"><span style={{ font: "1rem" }}>بارگذاری عکس</span></label>
                                                    </div>

                                                </div>
                                                {/* <div className="col-sm-12 col-md-6">
                                                    <div className="form-group row">
                                                        <label className="col-sm-4" for="formGroupExampleInput2">بارگذاری نمونه کار</label>
                                                        <div className="col-sm-8">
                                                            <input type="file" className="form-control-file" id="exampleFormControlFile1" />
                                                        </div>
                                                    </div>
                                                </div> */}
                                            </div>

                                           

                                            <div className="form-group row d-flex justify-content-left" style={{ marginRight: "10vw" }}>
                                                {/* <div className="col-md-6"> */}
                                                    <button type="submit" className="btn btn-primary ml-1">ثبت اطلاعات</button>
                                                {/* </div> */}
                                                {/* <div className="col-sm-6"> */}
                                                    <button type="submit" className="btn btn-danger"> انصراف</button>
                                                {/* </div> */}
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div id="tab-manageMyBusinesses" className="tab-pane manageMyBusinesses p-3" role="tabpanel" style={{ transition: "1s", marginTop: "0.5vh" }}>
                                <div className="row justify-content-center">
                                    <div className="col-md-8">
                                        <div className="panel panel-default panel-table rounded-lg">
                                            <div className="panel-heading"></div>
                                            <div className="panel-body">
                                                <table className="table table-striped table-bordered table-list text-right rounded-lg">
                                                    <thead>
                                                        <tr>
                                                            <th className="hidden-xs">ردیف</th>
                                                            <th>عنوان کسب و کار</th>
                                                            <th className="text-center">
                                                                <img src={setting} className="settingIcon" alt="" style={{ height: "2vh" }} />
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td className="hidden-xs">1</td>
                                                            <td>برنامه نویس فرانت-اند</td>
                                                            <td align="center">
                                                                <a href="#" className="p-2">
                                                                    <img src={edit} className="editIcon" alt="" style={{ height: "2vh" }} />
                                                                </a>
                                                                <a href="#" className="p-1">
                                                                    <img src={recyclingbin} className="recyclingbinIcon" alt="" style={{ height: "2vh" }} />
                                                                </a>
                                                                <a href="#" className="p-1">
                                                                    <img src={eye} className="eyeIcon" alt="" style={{ height: "2vh" }} />
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="panel-footer">
                                                <div className="row">
                                                    <div className="col col-xs-4 text-right">صفحه 1 از 1</div>
                                                    <div className="col col-xs-8">
                                                        <ul className="pagination hidden-xs float-left">
                                                            <li>
                                                                <a href="#">«</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">1</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">»</a>
                                                            </li>
                                                        </ul>
                                                        {/* <ul className="pagination visible-xs pull-left">

                                                        
                                                    </ul> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div className="tab-pane fade" id="v-pills-Request" role="tabpanel" aria-labelledby="v-pills-Request-tab">
                    <div id="minimal-tabs">

                        <ul className="nav nav-tabs">
                            <li className="nav-item">
                                <a className="nav-link" role="tab" data-toggle="tab" href="#TicketTab-1"> درخواست مشاوره</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" role="tab" data-toggle="tab" href="#TicketTab-2"> مشاهده مشاوره های دریافتی</a>
                            </li>
                        </ul>

                        <div className="tab-content">
                            <div id="TicketTab-1" className="tab-pane editProfile" role="tabpanel" style={{ transition: "1s", marginTop: "1vh" }}>
                                <div className="row justify-content-center">
                                    <div className="card col-md-7 text-right shadow rounded-lg">
                                        <form className="form p-5 text-right">
                                            <div className="form-group row">
                                                <select className="form-control mx-md-1 form-control-md-lg form-control-input" name="proviance" onChange={this.handleChange}>
                                                    <option value="رستۀ شغلی" selected="selected" disabled="disabled">رستۀ شغلی خود را انتخاب کنید</option>
                                                    <option value="Programmer">برنامه نویس</option>
                                                    {/* <option id="inputState" selected="selected" disabled="disabled" value="استان">استان</option>
                                                    {Proviances.map(item =>
                                                        <option key={item.id} value={item.provianceName}>{item.provianceName}</option>
                                                    )} */}
                                                </select>
                                            </div>
                                            <div className="form-group row">
                                                <label className="col-sm-4" for="formGroupExampleInput">عنوان</label>
                                                <div className="col-sm-8">
                                                    <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Example input" />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <select className="form-control mx-md-1 form-control-md-lg form-control-input" name="proviance" onChange={this.handleChange}>
                                                    <option value="رستۀ شغلی" selected="selected" disabled="disabled">اولویت</option>
                                                    <option value="high">زیاد</option>
                                                    <option value="middle">متوسط</option>
                                                    <option value="low">کم</option>
                                                </select>
                                            </div>


                                            <div className="form-group row">
                                                <label className="col-sm-4" for="formGroupExampleInput2">متن درخواست</label>
                                                <div className="col-sm-8">
                                                    <textarea type="text" className="form-control" rows="3" id="formGroupExampleInput2" placeholder="Another input"></textarea>
                                                </div>
                                            </div>

                                            <div className="row justify-content-center">
                                                <div className="col-md-4">
                                                    <input type="file" name="file" id="file" className="inputfile" />
                                                    <label for="file"><span style={{ font: "1rem" }}>بارگذاری فایل</span></label>
                                                </div>
                                            </div>

                                            <div className="form-group row d-flex justify-content-center float-left" style={{ marginRight: "10vw" }}>
                                                <button type="submit" className="btn btn-primary ml-3">ارسال</button>
                                                <button type="submit" className="btn btn-danger"> انصراف</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div id="TicketTab-2" className="tab-pane editResume" role="tabpanel" style={{ transition: "1s", marginTop: "0.5vh" }}>

                                <div className="row justify-content-center">
                                    <div className="col-md-8">
                                        <div className="panel panel-default panel-table rounded-lg">
                                            <div className="panel-heading"></div>
                                            <div className="panel-body">
                                                <table className="table table-striped table-bordered table-list text-right rounded-lg">
                                                    <thead>
                                                        <tr>
                                                            <th className="hidden-xs">ردیف</th>
                                                            <th>عنوان</th>
                                                            <th>تاریخ ارسال</th>
                                                            <th>تاریخ پاسخ</th>
                                                            <th>وضعیت</th>
                                                            <th className="text-center">
                                                                <img src={setting} className="settingIcon" alt="" style={{ height: "2vh" }} />
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td className="hidden-xs">1</td>
                                                            <td>چگونگی اضافه کردن کسب و کار جدید</td>
                                                            <td>1398/6/4</td>
                                                            <td>1398/6/5</td>
                                                            <td>در حال بررسی</td>
                                                            <td align="center">
                                                                <a href="#" className="p-2">
                                                                    <img src={reply} className="editIcon" alt="" style={{ height: "2vh" }} />
                                                                </a>
                                                                <a href="#" className="p-1">
                                                                    <img src={recyclingbin} className="recyclingbinIcon" alt="" style={{ height: "2vh" }} />
                                                                </a>
                                                                <a href="#" className="p-1">
                                                                    <img src={eye} className="eyeIcon" alt="" style={{ height: "2vh" }} />
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="panel-footer">
                                                <div className="row">
                                                    <div className="col col-xs-4 text-right">صفحه 1 از 1</div>
                                                    <div className="col col-xs-8">
                                                        <ul className="pagination hidden-xs float-left">
                                                            <li>
                                                                <a href="#">«</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">1</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">»</a>
                                                            </li>
                                                        </ul>
                                                        {/* <ul className="pagination visible-xs pull-left">

                                                        
                                                    </ul> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div >