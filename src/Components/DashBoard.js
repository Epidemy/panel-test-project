import React, { useState, useEffect } from 'react';

//Styles
// import './AdminPannel.css';

//Components


//Assets
import caretDown from "../Assets/Icons/caretDown.svg";
import category from "../Assets/Icons/category.svg";
import meter from "../Assets/Icons/meter.svg";
import repeat from "../Assets/Icons/repeat.svg";



const DashBoard = () => {

    const [Data, setData] = useState([]);    

    function fetchUrl(API){
        fetch(API)
            .then(response => response.json())
            .then(data => setData(data));
    }

    useEffect(() => {
        fetchUrl('https://api.jsonbin.io/b/5d3edbe435e3f814032c56d2');
    }, []);

    console.log("data", {Data})
    return (
        
        <div className="container">
            <div className="col-3 rightSidebarDropDown" style={{direction:"rtl"}}>
                <div className="tab-content" id="v-pills-tabContent">                    
                    <div className="tab-pane fade show active" id="v-pills-database" role="tabpanel" aria-labelledby="v-pills-database-tab" >
                        <h5 style={{float:"right",color:"white"}}>داده</h5><br/>
                        <ul className="databaseTabs nav flex-column nav-pills p-0 m-0" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <li className="databaseListItem nav-item dropdown">
                                <a className="databaseListTitle nav-link navbar-toggler" id="navbarTogglerCategory" data-toggle="collapse" href="#v-pills-database" role="tab" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="true" aria-label="Toggle navigation">
                                    <img src={caretDown} className="caretDownIcon" alt="caretDown" height="8vh"/>
                                    <span>دسته بندی </span>
                                </a><br/>
                                    <div className="navbar-collapse collapse float-right" id="navbarColor01" style={{direction:"rtl", flexBasis:"100%"}}>
                                        <ul className="navbar-nav mr-auto">
                                            {Data.map(item => {
                                                if(item.kind=="category"){
                                                    return(
                                                        <li className="nav-item">
                                                             <a className="nav-link float-right" href="#!">
                                                                <img src={category} className="categoryIcon" alt="category" height="8vh"/>
                                                                <span> {item.name} </span>
                                                             </a>
                                                        </li>
                                                    )
                                                }                                                
                                            })} 
                                        </ul>                                         
                                    </div>
                            </li>   

                            <li className="databaseListItem nav-item">
                                <a className="databaseListTitle nav-link navbar-toggler" id="navbarTogglerCategory" data-toggle="collapse" href="#v-pills-database" role="tab" data-target="#navbarColor02" aria-controls="navbarColor01" aria-expanded="true" aria-label="Toggle navigation">
                                    <img src={caretDown} className="caretDownIcon" alt="caretDown" height="8vh"/>
                                    <span>معیار </span>
                                </a><br />
                                    <div className="navbar-collapse collapse float-right" id="navbarColor02" style={{direction:"rtl", flexBasis:"100%", flexGrow:"1", alignItems:"center"}}>
                                        <ul className="navbar-nav mr-auto">
                                            {Data.map(item => {
                                                if(item.kind=="measure"){
                                                    return(
                                                        <li className="nav-item">
                                                            <a className="nav-link float-right" href="#!">
                                                                <img src={meter} className="meterIcon" alt="meterDown" height="9vh"/>
                                                                <span> {item.name} </span>
                                                            </a>
                                                        </li>
                                                    )
                                                }                                                
                                            })}
                                        </ul>
                                    </div>
                            </li>                            
                            
                            <li className="databaseListItem nav-item">
                                <a className="databaseListTitle nav-link navbar-toggler float-right" id="navbarTogglerCategory" data-toggle="collapse" href="#v-pills-database" role="tab" data-target="#navbarColor03" aria-controls="navbarColor01" aria-expanded="true" aria-label="Toggle navigation">
                                    <img src={caretDown} className="caretDownIcon" alt="caretDown" height="8vh"/>
                                    <span>معیار‌تجمعی </span>                                
                                </a><br/>
                                
                                    <div className="navbar-collapse collapse float-right" id="navbarColor03" style={{flexBasis:"100%"}}>
                                        <ul className="navbar-nav mr-auto">
                                            <li className="nav-item active">
                                                <a className="nav-link float-right" href="#!">
                                                    <img src={repeat} className="repeatIcon" alt="repeat" height="9vh"/>
                                                    <span> درصدتکرار </span>                                                 
                                                </a>
                                            </li>                                            
                                        </ul>                                        
                                    </div>
                                
                            </li>                           
                           
                        </ul>
                    </div>
                    <div className="tab-pane fade show" id="v-pills-things" role="tabpanel" aria-labelledby="v-pills-things-tab" >
                        <h5 style={{float:"right",color:"white"}}>اشیاء</h5><br/>
                    </div>
                    <div className="tab-pane fade show" id="v-pills-design" role="tabpanel" aria-labelledby="v-pills-design-tab" >
                       <h5 style={{float:"right",color:"white"}}>طرح‌کلی</h5><br/>
                    </div>
                </div>
            </div>

            <div className="col-9"> </div>

        </div>
        // <div className="tab-content" id="v-pills-tabContent" style={{ width: "100%" }}>
        //     <div className="tab-pane fade show active" id="v-pills-DataBase" role="tabpanel" aria-labelledby="v-pills-DataBase-tab" >
        //     </div>
        //     <div className="tab-pane fade" id="v-pills-Things" role="tabpanel" aria-labelledby="v-pills-Things-tab" >
        //     </div>
        //     <div className="tab-pane fade" id="v-pills-Design" role="tabpanel" aria-labelledby="v-pills-Design-tab" >
        //     </div>
        // </div>
    );
};
export default DashBoard;