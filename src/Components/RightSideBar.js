import React from 'react';

//Styles
import './SideBarStyle.css';

//Assets
import database from "../Assets/Icons/database.svg";
import things from "../Assets/Icons/things.svg";
import design from "../Assets/Icons/design.svg";
import settings from "../Assets/Icons/settings.svg";

const RightSideBar = () => {
    return (
        <nav className="navbar d-flex flex-md-row-reverse accordion bg-gradient-primary p-0 m-0">

            <div className="rightSidebar">
                <ul className="PanelTabs nav flex-column nav-pills p-0 m-0" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <li className="RightpanelListItem nav-item">
                            <a className="panelListTitle nav-link tablinks" id="v-pills-database-tab" data-toggle="pill" href="#v-pills-database" role="tab" aria-controls="v-pills-database" aria-selected="true">
                                <img src={database} className="databaseIcon" alt="database"/>
                                <p className="tabName">داده</p>
                            </a>
                        </li>
                        <li className="RightpanelListItem nav-item">
                            <a className="panelListTitle nav-link tablinks" id="v-pills-things-tab" data-toggle="pill" href="#v-pills-things" role="tab" aria-controls="v-pills-things" aria-selected="false">
                                <img src={things} className="thingsIcon" alt="things"/>
                                <p className="tabName">اشیاء</p>
                            </a>
                        </li>
                        <li className="RightpanelListItem nav-item">
                            <a className="panelListTitle nav-link tablinks" id="v-pills-design-tab" data-toggle="pill" href="#v-pills-design" role="tab" aria-controls="v-pills-design" aria-selected="false">
                                <img src={design} className="designIcon" alt="design"/>
                                <p className="tabName">طرح‌کلی</p>
                            </a>
                        </li>
                </ul>
            </div>
                        
        </nav>

    );
};
export default RightSideBar;