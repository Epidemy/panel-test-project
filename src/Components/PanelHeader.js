import React from 'react';
// import { Link } from "react-router-dom";

//Styles
// import './AdminPannel.css';

//Components


//Assets
import avatar from "../Assets/Images/avatar.jpg";
import menu from "../Assets/Icons/menu.svg";
// import searchIcon from "../Assets/Icons/searchIcon.svg";
import logOut from "../Assets/Icons/logOut.svg";
import bell from "../Assets/Icons/bell.svg";


const PanelHeader = () => {
    return (
        <div className="container-fluid">            

            <ul className="list-inline p-0" >
                <li className="list-inline-item float-right pt-3 pr-2">
                    <a href="#">
                        <img src={menu} className="menuIcon" alt="menu Icon"/>
                    </a>
                </li>
                <li className="list-inline-item dropdown no-arrow p-0 m-0">
                    <a className="nav-link" data-toggle="" href="#">
                        <img src={bell} className="bellIcon" alt="bell"/>
                    </a>                    
                </li>
                <li className="list-inline-item dropdown profileImg_Item p-0 m-0" role="presentation">
                    <a className="nav-link" href="#">
                        <img className="border rounded-circle profileImg" src={avatar}  alt="avatar"/>
                    </a>
                </li>
                <li className="list-inline-item p-0 m-0">
                    <a className="nav-link" data-toggle="" href="#">
                       <img src={logOut} className="logOut" alt="logOut"/>
                    </a>                    
                </li>
                
                <div className="d-none d-sm-block topbar-divider"></div>
                
            </ul>
        </div>

    );
};
export default PanelHeader;