import React from 'react';

//Styles
import './AdminPannelStyles.css';

//Components
import PanelHeader from './Components/PanelHeader';
import PanelNavBar from './Components/PanelNavBar';
import RightSideBar from './Components/RightSideBar';
import DashBoard from './Components/DashBoard';
import LeftSideBar from './Components/LeftSideBar';

function App() {
  return (
    <div className="container-fluid wholePanelPage py-4 px-5 m-0 shadow">

        <div className="panel">

            <div className="row panelHeader p-0 pb-2 m-0 shadow">
                <PanelHeader />
            </div>

            <div className="panelBody m-0 mx-auto p-0">

                <div className="panelNavBar row justify-content-center mt-3 mx-auto mb-0 p-0">
                    <PanelNavBar />
                </div>
                
                <div className="panelDetails row justify-content-between mx-0 mt-0 mb-3 p-0">
                    <div className="p-0">
                        <RightSideBar />
                    </div>
                    <div className="col-10 dashboard p-0 m-0" >                    
                        <DashBoard />                    
                    </div>
                    <div className="p-0">
                        <LeftSideBar />
                    </div>
                </div >

            </div>
        </div>

    </div>
  );
}

export default App;
